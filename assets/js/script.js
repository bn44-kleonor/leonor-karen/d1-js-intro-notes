/*********************************************************************************
   Module: WD004 JavaScript Programming
   Session 1: Intro to JavaScript

  A. WHAT IS JAVASCRIPT
      1) JS is the most popular programming language and serves as a great foundation for developers.
      2) JavaScript makes web sites / pages _______interactive________
      3) JS is our first "programming" language - this is the first time that we will write
      something that will tell our page ___what_to_do___ programmatically. */

/*********************************************************************************
  B. USING CONSOLE.LOG
    The console.log() method writes a message to the ________developers____________.
    The console is useful for _____testing_______ purposes. */
    console.log("Hello, World!");


    //JS is _____case-sensitive____________
    //console.Log('Hi,  World');

    //whitespaces may or may not be used as long as it doesn't change JS keywords
    console. log('Hi!');
    //although try to avoid this (unnecessary space)

/*********************************************************************************
  C. STATEMENTS & SYNTAX

    1) Statements
        - In a programming language, statements are ___instructions____ that we tell the computer to ___perform____. 
        - JS statements usually end with ____semicolon____. But it is NOT required.

    2) Syntax
        - JavaScript syntax is the set of __rules__ on how JavaScript programs are constructed.*/

/*********************************************************************************
  D. VARIABLES
    - are ___conatainers___ for ____data____.
    - A variable is a named location for storing a value. (from MDN)

    1) VARIABLE DECLARATIONS
      a. use the __let__ keyword followed by the variable name of your choosing and use the ___assignment___ operator (=) to assign a value.
      b. Variable names should start with a __small____ character. Use ___camel case___ for multiple words
      c. Variable names should be ___descriptive___ of the value being stored to avoid confusion. */

      let productName = "soap";
      let productId = 'S1234';
      console.log(productName, productId);

    //the above set of variable declarations can be shortened to:
      let productName2 = "soap2", productId2 = "S2345";
      console.log(productName2, productId2);


    //variables can be called in other statements



  //2) VARIABLE ERRORS

    //Example 1
      //console.log(productNme);
      //console.log(product);


      //The above code will result in an "_____uncaught reference error______: productNme is not defined".
      //This is means that the variable specified was not given a value (aka____not initialized____) prior to being called.
      //Besides misspelling, this could also be the result of simply forgetting to give a variable a value.

    //Example 2
      //let class = "Batch 44";
      //console.log(class);

      //Uncaught _____Syntax Error_______: "Unexpected token class"
      //This is because the word class is a ____reserved____ keyword and cannot be used as a variable name

      let className ="Batch 44";
      console.log(className);
      //The above will work as className is NOT a ____reserved___ keyword in JS


  /*********************************************************************************

  E. DATA TYPES
      The type of data that a ___variable___ holds. In our example a while ago,
      let className = "Batch 23", the className is holding a string data. 
      Data Types fall into two categories: _____Primitive____ Data Types and ____Objects____.

    1) Primitive data types
      a) String
          - includes ____alphanumeric____ characters and _____special_____ characters*/
          //- has to be enclosed in either single, double ____quotes_____, or _____back ticks_____.
          let name = "John Doe";
          let name2 = 'Jane Doe';
          let name3 = `Play Doe`;
          console.log(name, name2, name3);


          //- backslash \ can be used in a string as an ___escape___ character. */
        let sentence = "Mark said, \"Hello\".";
        console.log(sentence);
        sentence = 'John said, "Hello".';
        console.log(sentence);
        sentence = `${name} said,"Hello".`;
        console.log(sentence);


          // other escape characters include: \n and \t
        sentence = "Mark said,\n \"Hello\".";
        console.log(sentence);
        sentence = "Mark said,\t \"Hello\".";
        console.log(sentence);
        

          //string has a .length property.
        let myName = "Karen Leonor";
        console.log(myName.length);
         

          //strings can be concatenated using +
         console.log(myName + " " + name3); 
         console.log("Hi, my name is " + myName + "!"); //simple quotes
         console.log(`Hi, my name is ${myName} !`); //EXTENDED FUNCTIONALITY QUOTE (allows you to embed formula)


      /*b) Number/Numeric
          - Includes all possible numerical values including the special NaN, positive infinity, & negative infinity.*/
         let num = Infinity;
         console.log(num);    //direct reference
         console.log(-1/0);   //

          /*- Numbers are ___NOT___ to be enclosed in single nor double quotes.
          - NaN is generated when a math function fails (Math.abs("hello")) or when a function trying to 
            parse a number fails (Number("hello")).
          - NaN is the only value in JS that does ______________ equal itself.
          - Numbers are used for computation */
          console.log(typeof 1);
          console.log( typeof "1");
          console.log(1 + 1);
          console.log("1" + 1);

      /*c) Boolean
          - Holds either ____true___ or ___false___ */

          let ageFieldChecked = true;
          let nameFieldChecked = false;
          console.log(nameFieldChecked);
          console.log(ageFieldChecked);


      /*d) Undefined
          - Represents the state of a variable that's been __declared__ but __without___ an assigned value */
          let lastName;
          console.log(lastName);
        
      
      /* e) Null
          - Used to INTENTIONALLY express the ___absence____ of a value in a variable declaration */
          let x = null;
          console.log(x);


        
    /*2. Objects
        - Objects are variables too. But objects can contain ___MULTIPLE___ values.
        - The values are written as name : value _____pairs_____ (name and value separated by a colon). 
        - A JavaScript object is A COLLECTION of named values (aka properties).
        - ____Properties____ can be any of the previously mentioned data types as well as other objects, and functions. */

        let person = {
          name: "John Doe",
          age: 30,
          isEmployed: true
        };

        console.log(person);
        console.log(typeof person);

         
      /*To access the properties of an object, we use the dot notation.*/
        console.log(person.name);
        console.log(person.age);
        console.log(person.isEmployed);
        console.log(person.name, person.age, person.isEmployed);


  /*********************************************************************************
  /*********************************************************************************

  ACTIVITY 1
  1. Create s1 folder inside WD004 folder
  2. Inside s1, create a1-js-variables folder
  3. Inside a1-js-variables, create index.html and assets folder
  4. Inside assets folder, create js folder.
  5. Inside js folder, create script.js folder.
  6. Open your a1-js-variables folder in Sublime.
  7. Load script.js in index.html
  8. On your script.js file, write a code to display the following on your console:

    Jake asks Finn, “Hey, let’s go on an adventure?”

  9. Determine the correct data type of the following:

    a. let num = “1”;
    b. let weightInPounds = 261.30;
    c. let x;
    d. let length = `22.1 cm`;

  10. Create a a1-js-variables folder inside your b44 repo and push the contents of your a1-js-variables folder.
    a. Make sure that you are inside a1-js-variables 
    b. git status
    c. git remote add origin fsafsafdsafdsafsafsaLINKfdafdsafsa
    d. git add -A
    e. git commit -m "initial commit"
    f. git push origin master


  /*********************************************************************************
  /*********************************************************************************


  F. ASSIGNMENT OPERATORS

    1) Basic Assignment Operator (=)
        - _____Assigns____ a value to a variable
        - right to left */
        let batch44Students = 24;

    /* 2) Addition assignment Operator (+=)
        - adds the value of the right operand to a variable and assigns the result to the same variable.*/
        //batch44Students = batch44Students + 2;
        batch44Students += 2;
        console.log(batch44Students);
      
      /*The code above will output __26__

    3) Subtraction Assignment Operator (-=) */
      batch44Students -= 2;   //batch44Students = batch44Students - 2;
      console.log(batch44Students);
     
      /*The code above will output __24__

    4) Multiplication Assignment Operator (*=) */
      batch44Students *= 2;
      console.log(batch44Students);
      
      /*The code above will output __48__

    5) Division Assignment Operator (/=) */
      batch44Students /= 2;
      console.log(batch44Students);
 
      /*The code above will output __24__

    6) Remainder Assignment or Modulo Operator (%=) */   
      batch44Students %= 2;
      console.log(batch44Students);

      batch44Students = 15;
      batch44Students %= 2;
      console.log(batch44Students);

      /*The code above will output __ */


      /*The code above will output __

      ==================================================================

      NOTES: Multiplication and Division are always performed first before
      addition and subtraction (MDAS rule in math) */
      let answer = 3+4*5;
      console.log(answer);

      //The code will output ______ due to MDAS rule in math


      answer = (3+4)*5;
      console.log(answer);
      //use () to ______ the default priority given to * and /

/*********************************************************************************

  G. JS IS A LOOSELY-TYPED LANGUAGE
      Though JS recognizes type, you don't have to explicitly declare a variable's type upon declaration. 
      You can also change a variable's type on the fly. The only way to set a variable's 
      type in JS is to assign a value of that type to the variable.


      Danger with Loose Typing 

      When JS sees a string in an operation, it converts everything to a ____string_____.
      As a rule of thumb, if your mathematical operations in JS are giving out erroneous results,
      check that your numbers are being stored as numbers, not strings*/



 /*********************************************************************************
  H. ARITHMETIC OPERATORS
      - mathematical operations are carried out using the following:
        + for addition
        - for subtraction
        * for multiplication
        / for division
        % for remainder
        () for overriding default operational priority */

        let productPrice = 21.5;
        let productQuantity = 10;
        let totalAmount = productPrice * productQuantity;
        console.log(totalAmount);

        let remainder = 13 % 6;
        console.log(remainder); // 13 / 6 equal 2 remainder 1

        //multiplication and division are always performed first before addition and subtraction (MDAS rule in math)
       

        //use () to override the default priority given to * and /




/*********************************************************************************
  I. INCREMENT & DECREMENT OPERATORS

    1) Increment Opeartor (++)
        - adds one to its operand and returns a value. */

        // POSTFIX (x++) returns the value BEFORE incrementing
        let a = 3;
        let b = a++;
        console.log('Increment - PostFix')
        console.log(b); //3
        console.log(a); //4

        // PREFIX (++x) returns the value AFTER incrementing
        let c = 3;
        let d = ++c;
        console.log('Increment - PREFix')
        console.log(c); //4
        console.log(d); //4

  /*2) Decrement Opeartor (--)
        - subtracts one to its operand and returns a value. */
        // POSTFIX (x--) returns the value BEFORE decrementing
        a = 3;
        b = a--;
        console.log('Increment - PostFix')
        console.log(b); //3
        console.log(a); //2

        // PREFIX (--x) returns the value AFTER decrementing
        c = 3;
        d = --c;
        console.log('Increment - PREFix')
        console.log(c); //2
        console.log(d); //2



/********************************************************************************

  J. TYPE CONVERSION (aka TYPE COERCION)
      - When JS is given an operation with conflicting data types, it tries to normalize them first
      before performing the operation.
      - Boolean will be converted to a number if compared with a number.
      - Number will be converted to string if a string is involved.


  K. COMPARISON OPERATORS
      - Used to compare 2 values and return either true or false.
    1) strict comparison
        - is only true if the operands are of the same data type and the contents match.
        - performs no type conversion */

        //a) STRICT equality operator (===)  [data type is crucial]
         console.log("Strict Equality Comparison");
         console.log(1 === 1); //true
         console.log("1" === 1); //false
         console.log(1 === true); //false [diff. data type - number(1) and boolean(true)]
         console.log("" === 0); //false  [diff. data type]


        //b) Strict Inequality Operator (!==)
         console.log("Strict Inequality Comparison");
         console.log(1 !== 1); //false
         console.log("1" !== 1); //true
         console.log(1 !== true); //true
         console.log("" !== 0); //true


    /*2) abstract comparison (type coersion is involved)
        - converts the operands to the same type before making the comparison.*/

         //a) Equality operator (==)  [not sensitive to data type]
         console.log("Equality Comparison");
         console.log(1 == 1); //true
         console.log("1" == 1); //true
         console.log(1 == true); //true
         console.log("" == 0); //true
        

        //b) Inequality Operator (!=)
        // returns true if operands are not equal
         console.log("Inequality Comparison");
         console.log(1 != 1); //false
         console.log("1" != 1); //false
         console.log(1 != true); //false
         console.log("" != 0); //false
              

/*********************************************************************************
  /*********************************************************************************

  ACTIVITY 2
  1. Create s1 folder inside WD004 folder
  2. Inside s1, create a2-js-operators folder
  3. Inside a2-js-operators, create index.html and assets folder
  4. Inside assets folder, create js folder.
  5. Inside js folder, create script.js folder.
  6. Open your a2-js-operators folder in Sublime.
  7. Load script.js in index.html
  8. On your script.js file, use the remainder operator to check if a variable is odd or even.

  9. Create a a1-js-variables folder inside your b44 repo and push the contents of your a1-js-variables folder.
    a. Make sure that you are inside a1-js-variables 
    b. git status
    c. git remote add origin fsafsafdsafdsafsafsaLINKfdafdsafsa
    d. git add -A
    e. git commit -m "initial commit"
    f. git push origin master


  /*********************************************************************************
/********************************************************************************

  L. RELATIONAL OPERATORS
      Note: Type conversion occurs prior to comparison. 

      1) Greater Than (>)*/
      console.log("Greater Than Operator");
      console.log(0 > 1); // false
      console.log(2 > 1); // true
      console.log(2 > ""); // true   - (type coercion/conversion) "" converts to 0
       

    //2) Greater than or Equal to (>=)
      console.log("Greater Than OR Equal To");
      console.log(0 >= 1); // false
      console.log(1 >= 1);  // true
      console.log("1" >= 1); // true  - (type coercion/conversion) "1" converst to 1


    //3) Less Than (<)
      console.log("Less Than Operator");
      console.log(0 < 1); // true
      console.log(2 < 1);  // false
      console.log("2" < ""); // false  - (type coercion/conversion) "" converst to 0



    //4) Less than or Equal to (<=)
      console.log("Less Than OR Equal To");
      console.log(0 <= 1); // true
      console.log(1 <= 1);  // true
      console.log("1" <= 1); // true  - (type coercion/conversion) "1" converst to 1



/*********************************************************************************


  M. FUNCTIONS
      - a way of grouping a set of operations under a given name so that they can be called __repeatedly__ */
      function saySomething () {
        console.log("Hello there!");
      }

      saySomething();
      saySomething();
      saySomething();
      saySomething();
      
      //the use of () is for passing in values as arguments into the function
      function saySomething2(msg) {
        //console.log(msg + ", my name is Karen Leonor");
        console.log(`${msg}, my name is Karen Leonor"`);
      }

      saySomething2("Hi");
      saySomething2("Hello");
      saySomething2("Goodbye");


    /*1) SCOPE
      - variables can exist in either 1 of 2 scopes:
        a) ________ scope - not declared within a function, available everywhere */
        

          //calling fullName executes alertFullName inside it which puts together the lastName 
          //declared within it and the firstName from its parent function



       //b) ________ scope - declared within a function, only available within it 
          //a function has access to all variables declared above it, no matter how deeply it's nested
          //to illustrate, see below example:
          //declare a global variable and giving it the value "a"
         

          //expected output of the above:
          /*leveld a b c d
          levelc a b c
          levelb a b
          global a*/




    /*2) HOISTING
      - JS moves all variable declarations to the top of a function*/
      
     
    //take note that it only moves the declaration and NOT the assignment
    //it might seem that we should get the value "Emma" on our examples but an undefined variable with the same name is declared within the function, effectively taking precedence over it




    /*3) DECLARATION
        - always declare function declarations at the top of your code's scope to avoid hoisting
        - if using function expressions, declare your variables at the top of your local scope
        - the reason for the 2 "rules" above is so that we can avoid hoisting
        - we avoid hoisting because it often leads to inconsistent behavior across browsers*/

    /*4) ARGUMENTS 
        - defining arguments when defining a function declares variables for use within the scope of that function*/








